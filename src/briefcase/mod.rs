use m3rs_yew::{
	color::ColorRole,
	components::buttons::filled::FilledButton,
	components::buttons::filled_tonal::FilledTonalButton,
	components::buttons::outlined::OutlinedButton,
	typescale::{TypescaleModifier, TypescaleRoles},
	typography::Typography,
};
use reqwest_wasm::Client;
use serde::{Deserialize, Serialize};
use yew::prelude::*;

pub enum Msg {
	GetBriefcase,
	SetBriefcase(Projects),
}

pub struct Briefcase {
	projects: Projects,
}

impl Component for Briefcase {
	type Message = Msg;
	type Properties = ();

	fn create(ctx: &Context<Self>) -> Self {
		ctx.link().send_message(Msg::GetBriefcase);
		Self {
			projects: Projects { projects: vec![] },
		}
	}

	fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
		match msg {
			Msg::GetBriefcase => {
				ctx.link().send_future(async {
					let briefcase = get_projects().await;
					Msg::SetBriefcase(briefcase)
				});

				true
			}
			Msg::SetBriefcase(projects) => {
				self.projects = projects;
				true
			}
		}
	}

	fn view(&self, _: &Context<Self>) -> Html {
		html! { <div class={"projects-section"}>
			{  self.projects.projects
				.iter()
				.map(|p| html!{ < ProjectCard project={p.clone()} /> } )
				.collect::<Html>()
		} </div> }
	}
}

async fn get_projects() -> Projects {
	let url = "https://gitlab.com/api/v4/projects/48214214/repository/files/briefcase.toml/raw";

	let res = Client::new().get(url).send().await.unwrap();

	toml::from_str::<Projects>(&res.text().await.unwrap()).unwrap()
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Projects {
	projects: Vec<Project>,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq, Eq)]
pub enum ProjectStatus {
	Active,
	Inactive,
	Abandoned,
	Unreleased,
}

#[derive(Debug, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct Project {
	pub name: String,
	pub repository: String,
	pub package: Option<String>,
	pub website: Option<String>,
	pub description: String,
	pub status: ProjectStatus,
	pub tags: Vec<String>,
	pub image: Option<String>,
}

#[derive(Debug, PartialEq, Properties)]
pub struct ProjectCardProps {
	project: Project,
}

#[function_component(ProjectCard)]
pub fn project_card(
	ProjectCardProps {
		project:
			Project {
				name,
				repository,
				package,
				website,
				description,
				status,
				tags,
				image,
			},
	}: &ProjectCardProps,
) -> Html {
	let image = if let Some(image) = image {
		html! { <img src={image.clone()} /> }
	} else {
		html! {}
	};

	let repository = html! { <span class={"project-link"}>
		<a href={repository.clone()} target={"_blank"} >
			<FilledButton label={"code"} icon={"code"}  />
	</a></span> };

	let package = if let Some(package) = package {
		html! { <span class={"project-link"}><a href={package.clone()} target={"_blank"} >
			<FilledTonalButton label={"package"} icon={"deployed_code"}  />
		</a></span> }
	} else {
		html! {}
	};

	let website = if let Some(website) = website {
		html! { <span class={"project-link"}><a href={website.clone()} target={"_blank"} >
			<OutlinedButton label={"website"} icon={"web"}  />
		</a></span> }
	} else {
		html! {}
	};

	html! { <div class={"project-container"} >
		<div class={"project-name"} >
			<Typography text={name.clone()} role={TypescaleRoles::Headline} />
		</div>

		<div class={"project-status"}>
			<Status status={*status} />
		</div>

		<div class={"project-image"}>{image}</div>

		<div class={"project-tags"} >{
			tags.iter().map(|tag| html!{ <Tag  text={tag.clone()} /> } ).collect::<Html>()
		}</div>

		<div class={"project-description"} >
			<Typography text={description.clone()} role={TypescaleRoles::Body} />
		</div>

		<div class={"project-links"} >
			{repository}
			{package}
			{website}
		</div>

	</div> }
}

#[derive(Debug, PartialEq, Properties)]
pub struct StatusProps {
	pub status: ProjectStatus,
}

#[function_component(Status)]
pub fn status(StatusProps { status }: &StatusProps) -> Html {
	let status = match status {
		ProjectStatus::Active => {
			html! {<span class={"status-span"} >
				<span class={"status-circle green"} />
				<Typography
					text={"active"}
					role={TypescaleRoles::Label}
					modifier={TypescaleModifier::Small}
					color={ColorRole::Primary10}
				/>
			</span> }
		}
		ProjectStatus::Inactive => {
			html! {<span class={"status-span"} >
				<span class={"status-circle yellow"} />
				<Typography
					text={"inactive"}
					role={TypescaleRoles::Label}
					modifier={TypescaleModifier::Small}
					color={ColorRole::Primary10}
				/>
			</span> }
		}
		ProjectStatus::Abandoned => {
			html! {<span class={"status-span"} >
				<span class={"status-circle red"} />
				<Typography
					text={"abandoned"}
					role={TypescaleRoles::Label}
					modifier={TypescaleModifier::Small}
					color={ColorRole::Primary10}
				/>
			</span> }
		}
		ProjectStatus::Unreleased => {
			html! {<span class={"status-span"} >
				<span class={"status-circle blue"} />
				<Typography
					text={"unreleased"}
					role={TypescaleRoles::Label}
					modifier={TypescaleModifier::Small}
					color={ColorRole::Primary10}
				/>
			</span> }
		}
	};

	html! { <span class={"status-container"}>
		<span class={"status"}>
			<Typography
				text={"current status:&nbsp;"}
				role={TypescaleRoles::Label}
				modifier={TypescaleModifier::Small} />
			{status}
		</span>
	</span> }
}

#[derive(Debug, PartialEq, Properties)]
pub struct TagProps {
	pub text: AttrValue,
}

#[function_component(Tag)]
pub fn tag(TagProps { text }: &TagProps) -> Html {
	html! {<span class={"tag"}>
		{text}
	</span>}
}
