#![recursion_limit = "256"]
use wasm_bindgen::prelude::*;

mod app;
mod blog;
mod bottombar;
mod briefcase;
mod git2p;
mod gitlab;
mod home;
mod icon_button;
mod sidenav;
mod switch;
mod utils;

#[wasm_bindgen(start)]
pub fn yewterial() -> Result<(), JsValue> {
	wasm_logger::init(wasm_logger::Config::default());
	yew::Renderer::<app::App>::new().render();
	Ok(())
}
