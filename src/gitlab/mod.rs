use serde::Deserialize;
use wasm_bindgen::JsCast;
use wasm_bindgen_futures::JsFuture;
use web_sys::{Request, RequestInit, RequestMode, Response};

#[derive(Deserialize)]
pub struct TreeEntry {
	id: String,
	name: String,
	#[serde(rename(deserialize = "type"))]
	_kind: EntryKind,
	path: String,
	#[serde(rename(deserialize = "mode"))]
	_mode: EntryMode,
}

#[derive(Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum EntryKind {
	Tree,
	Blob,
}

#[derive(Deserialize)]
pub enum EntryMode {
	#[serde(rename = "100644")]
	Entry,
	#[serde(rename = "100644")]
	Tree,
	#[serde(rename = "040000")]
	Executable,
	#[serde(rename = "120000")]
	Symblink,
}

#[derive(Debug, Deserialize)]
struct Blame {
	commit: Commit,
	lines: Vec<String>,
}

#[derive(Debug, Deserialize)]
struct Commit {
	authored_date: String,
	author_name: String,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Entry {
	pub id: String,
	pub name: String,
	pub author_name: String,
	pub authored_date: String,
	pub content: String,
}

pub struct Client {
	api_url: String,
}

impl Default for Client {
	fn default() -> Self {
		Self {
			api_url: String::from("https://gitlab.com/api/v4"),
		}
	}
}

impl Client {
	pub async fn collect_markdown_files() -> Result<Vec<Entry>, ()> {
		let mut entries: Vec<Entry> = vec![];

		match Client::fetch_tree().await {
			Ok(tree) => {
				for entry in tree {
					if entry.name.ends_with(".md") {
						match Client::fetch_blame(entry).await {
							Ok(raw) => entries.push(raw),
							_ => return Err(()),
						}
					}
				}
			}
			_ => return Err(()),
		}

		Ok(entries)
	}

	async fn fetch_blame(entry: TreeEntry) -> Result<Entry, ()> {
		let mut opts = RequestInit::new();
		opts.method("GET");
		opts.mode(RequestMode::Cors);

		let request = Request::new_with_str_and_init(
			&Client::default().url(&format!(
				"/projects/{}/repository/files/{}/blame?ref=master",
				Repository::default().id,
				entry.path,
			)),
			&opts,
		)
		.unwrap();

		let window = gloo_utils::window();
		let resp_value = JsFuture::from(window.fetch_with_request(&request))
			.await
			.unwrap();

		let resp: Response = resp_value.dyn_into().unwrap();

		let text = JsFuture::from(resp.text().unwrap()).await.unwrap();

		let blame: Vec<Blame> = serde_json::from_str(&text.as_string().unwrap()).unwrap();

		Ok(Entry {
			id: entry.id,
			name: entry.name,
			authored_date: blame[0].commit.authored_date.to_owned(),
			author_name: blame[0].commit.author_name.to_owned(),
			content: blame[0].lines.join("\n"),
		})
	}

	pub async fn fetch_tree() -> Result<Vec<TreeEntry>, ()> {
		let mut opts = RequestInit::new();
		opts.method("GET");
		opts.mode(RequestMode::Cors);

		let request = Request::new_with_str_and_init(
			&Client::default().url(&format!(
				"/projects/{}/repository/tree?recursive=true",
				Repository::default().id
			)),
			&opts,
		)
		.unwrap();

		let window = gloo_utils::window();
		let resp_value = JsFuture::from(window.fetch_with_request(&request))
			.await
			.unwrap();

		let resp: Response = resp_value.dyn_into().unwrap();

		let text = JsFuture::from(resp.text().unwrap()).await.unwrap();

		Ok(serde_json::from_str(&text.as_string().unwrap()).unwrap())
		//Ok(text.as_string().unwrap())
	}

	fn url(&self, resource: &str) -> String {
		if resource.starts_with('/') {
			return format!("{}{}", self.api_url, resource);
		}
		format!("{}/{}", self.api_url, resource)
	}
}

struct Repository {
	id: String,
}

impl Default for Repository {
	fn default() -> Self {
		Self {
			id: String::from("34716768"),
		}
	}
}
