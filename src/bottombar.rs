use crate::icon_button::{IconButton, IconKind};
use yew::prelude::*;

#[function_component(Bottombar)]
pub fn bottombar() -> Html {
	let gitlab = {
		let link = html! { <a href={"https://gitlab.com/0al3x"} target={"_blank"}>
			<i class="fab fa-gitlab icon"></i>
		</a> };

		html! { <div class={classes!("contact-icon")}> <IconButton icon={IconKind::Link(link)} /> </div> }
	};

	let github = {
		let link = html! { <a href={"https://github.com/04l3x"} target={"_blank"}>
			<i class="fab fa-github icon"></i>
		</a>};

		html! { <div class={classes!("contact-icon")}> <IconButton icon={IconKind::Link(link)} /> </div> }
	};

	let telegram = {
		let link = html! { <a href={"https://t.me/al3x00"} target={"_blank"}>
			<i class="fab fa-telegram icon"></i>
		</a>};

		html! { <div class={classes!("contact-icon")}> <IconButton icon={IconKind::Link(link)} /> </div> }
	};

	let discord = {
		let link = html! { <a href={"https://discordapp.com/users/728754894328037478"} target={"_blank"}>
			<i class="fab fa-discord icon"></i>
		</a>};
		html! { <div class={classes!("contact-icon")}> <IconButton icon={IconKind::Link(link)} /> </div> }
	};

	html! { <div id={"bottombar"} >
		<div class={classes!("contact-icons") }>
			{gitlab}
			{github}
			{telegram}
			{discord}
		</div>
	</div> }
}
