use yew::prelude::*;

#[function_component(Home)]
pub fn home() -> Html {
	html! { <div class={classes!("home-container")} >
			<div class={classes!("code-snippet")}>
				<span class={classes!("font-hack")}>
					<div>
						<span class={classes!("red")} >
							{"assert!("}
						</span>
					</div>

					<div>
						<span class={classes!("blue", "tab")} >
							{"\tyou_will_never_be_right_here_right_now_again"}
						</span>
						<span class={classes!("red")} >
							{"()"}
						</span>
					</div>

					<div>
						<span class={classes!("green", "tab")} >
							{"\t\t&&"}
						</span>
					</div>

					<div>
						<span class={classes!("blue", "tab")} >
							{"\tyou_can_only_always_be_right_here_right_now"}
						</span>
						<span class={classes!("red")} >
							{"()"}
						</span>
					</div>

	//  	    	<div>
	//		    		<span class={classes!("red", "tab")} >
	//		    			{"\t,"}
	//		    		</span>
	//		    	</div>
	//
	//		    	<div>
	//		    		<span class={classes!("cyan", "tab")} >
	//		    			{"\ttrue"}
	//		    		</span>
	//		    	</div>
	//
					<div>
						<span class={classes!("red")} >
							{");"}
						</span>
					</div>
				</span>
			</div>
		</div> }
}
