use m3rs_yew::icons::Icon;
use yew::{prelude::*, virtual_dom::VNode};

#[derive(Debug, Clone, PartialEq)]
pub enum IconKind {
	Font(AttrValue),
	_Svg(VNode),
	Link(VNode),
}

#[derive(Debug, Clone, PartialEq, Properties)]
pub struct IconButtonProps {
	pub icon: IconKind,
}

#[function_component(IconButton)]
pub fn icon_button(IconButtonProps { icon }: &IconButtonProps) -> Html {
	match icon {
		IconKind::_Svg(node) => html! { <div class={classes!("icon","icon-container")}>
			{node.clone()}
		</div> },

		IconKind::Font(icon) => html! { <div class={classes!("icon-container")} >
			<Icon {icon} />
		</div> },

		IconKind::Link(node) => html! { <div>
			{node.clone()}
		</div> },
	}
}
