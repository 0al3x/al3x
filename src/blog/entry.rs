use crate::gitlab::Entry;
use m3rs_yew::components::buttons::filled_tonal::FilledTonalButton;
use m3rs_yew::typescale::{TypescaleModifier, TypescaleRoles};
use m3rs_yew::typography::Typography;
use yew::prelude::*;

pub struct BlogEntry;

#[derive(Properties, PartialEq)]
pub struct EntryProps {
	pub entry: Entry,
}

impl Component for BlogEntry {
	type Message = ();
	type Properties = EntryProps;

	fn create(_ctx: &Context<Self>) -> Self {
		Self {}
	}

	fn view(&self, ctx: &Context<Self>) -> Html {
		let props = &ctx.props();
		let entry = props.entry.clone();

		html! { <div class={"entry"} >
			<div class={"entry-title"} >
				<Typography role={TypescaleRoles::Title} text={{entry.name}} />
			</div>

			<div class={"entry-headline"}>
				<Typography
					role={TypescaleRoles::Label}
					modifier={TypescaleModifier::Large}
					text={entry.author_name}
				/>
				<div />
				<Typography
					role={TypescaleRoles::Label}
					modifier={TypescaleModifier::Small}
					text={entry.authored_date}
				/>
			</div>

			<div  class={"entry-body"}>
				<Typography role={TypescaleRoles::Body} text={entry.content} />
			</div>

			<div class={"entry-footer"}>
				<FilledTonalButton icon={"local_library"} label={"open"} />
			</div>

		</div>}
	}
}
