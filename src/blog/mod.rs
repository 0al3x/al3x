use crate::{
	blog::entry::BlogEntry,
	gitlab::{Client, Entry},
};
use yew::prelude::*;

mod entry;

pub struct Blog {
	entries: FetchState<Vec<Entry>>,
}

pub enum Msg {
	SetEntriesFetchState(FetchState<Vec<Entry>>),
	GetEntries,
}

pub enum FetchState<T> {
	NotFetching,
	Fetching,
	Success(T),
	Failed,
}

impl Component for Blog {
	type Message = Msg;
	type Properties = ();

	fn create(ctx: &Context<Self>) -> Self {
		ctx.link().send_message(Msg::GetEntries);

		Self {
			entries: FetchState::NotFetching,
		}
	}

	fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
		match msg {
			Msg::SetEntriesFetchState(fetch_state) => {
				self.entries = fetch_state;
				true
			}
			Msg::GetEntries => {
				ctx.link().send_future(async move {
					match Client::collect_markdown_files().await {
						Ok(entries) => Msg::SetEntriesFetchState(FetchState::Success(entries)),
						Err(_) => Msg::SetEntriesFetchState(FetchState::Failed),
					}
				});
				ctx.link()
					.send_message(Msg::SetEntriesFetchState(FetchState::Fetching));
				false
			}
		}
	}
	fn view(&self, _: &Context<Self>) -> Html {
		match &self.entries {
			FetchState::NotFetching => html! {<></>},
			FetchState::Fetching => {
				html! { <div class={classes!("spinner-ctn")}> <div class={classes!("spinner")} /> </div> }
			}
			FetchState::Success(data) => html! {
				<div id={"blog"} > {
					data.iter().map(|entry| {
						html!{ <BlogEntry key={entry.id.clone()} entry={entry.clone()} /> }
					}).collect::<Html>()
				}</div>
			},

			FetchState::Failed => {
				html! { <>{"Something went wrong at loading blog entries..."}</> }
			}
		}
	}
}
