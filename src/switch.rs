use yew::prelude::*;
use yew_router::prelude::*;

use crate::blog::Blog;
use crate::briefcase::Briefcase;
use crate::git2p::Git2P;
use crate::home::Home;

#[derive(Clone, Routable, PartialEq)]
pub enum Routes {
	#[at("/blog")]
	Blog,
	#[at("/briefcase")]
	Briefcase,
	#[at("/git2p-project")]
	Git2P,
	#[at("/")]
	Home,
}

#[allow(dead_code)]
pub fn switch(routes: Routes) -> Html {
	match routes {
		Routes::Home => html! { <Home /> },
		Routes::Blog => html! { <Blog /> },
		Routes::Git2P => html! { <Git2P /> },
		Routes::Briefcase => html! { <Briefcase /> },
	}
}
