use wasm_bindgen::JsValue;
use web_sys::{window, Document, HtmlBaseElement, Node};

pub fn set_base() {
	let document = window().unwrap().document().unwrap();

	let base_elem = HtmlBaseElement::from(JsValue::from(document.create_element("base").unwrap()));

	base_elem.set_href(&origin(&document));

	let head = document.head().unwrap();

	head.append_child(&Node::from(base_elem)).unwrap();
}

fn origin(document: &Document) -> String {
	let location = document.location().unwrap();

	let origin = location.origin().unwrap();

	if location.hostname().unwrap().eq("localhost") {
		format!("{}/al3x/", origin)
	} else {
		format!("{}/al3x", origin)
	}
}
