use wasm_bindgen::JsCast;

#[allow(unused)]
pub fn element<T: JsCast>(id: &str) -> T {
	web_sys::window()
		.unwrap()
		.document()
		.unwrap()
		.get_element_by_id(id)
		.unwrap()
		.dyn_into::<T>()
		.unwrap()
}
