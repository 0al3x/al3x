#![allow(clippy::just_underscores_and_digits)]

// use crate::switch::*;
use crate::utils::html;
// use crate::{bottombar::Bottombar, sidenav::Sidenav};
use gloo_console::log;
use gloo_events::EventListener;
use m3rs_yew::components::buttons::elevated::ElevatedButton;
use yew::prelude::*;
// use yew_router::prelude::*;

pub struct App {
	__: EventListener,
}

pub enum Message {
	ConsoleMessage,
}

impl Component for App {
	type Message = Message;
	type Properties = ();

	fn create(ctx: &Context<Self>) -> Self {
		html::set_base();

		let doc = web_sys::window().unwrap().document().unwrap();

		let style = doc.create_element("style").unwrap();
		style.set_inner_html(include_str!(concat!(env!("OUT_DIR"), "/index.css")));

		doc.head().unwrap().append_child(&style).unwrap();

		m3rs_yew::config().unwrap();

		let cb = ctx.link().callback(|_: Event| Message::ConsoleMessage);

		let __ = EventListener::new(&web_sys::window().unwrap(), "devtools-opened", move |e| {
			cb.emit(e.clone())
		});

		Self { __ }
	}

	fn view(&self, _: &Context<Self>) -> Html {
		html! { <>
			<div class={"container"}>
				<span class={"span"}>{"maybe, my freedom lies in a shot"}</span>
			</div>
			/*<BrowserRouter>
				<div>
					<div id={"drawer-nav"} >
						<Sidenav />
					</div>
					<div id={"drawer-container"} >
						<div id={"drawer-spacer"} />
						<div id={"drawer-viewer"}  >
							<Topbar />
							<div id={"topbar-spacer"} />
							<div id={"view"} >
								<Switch<Routes> render={switch } />
							</div>
						</div>
					</div>
				</div>
				<Bottombar />
			</BrowserRouter>*/
		</> }
	}

	fn update(&mut self, _: &Context<Self>, msg: Self::Message) -> bool {
		match msg {
			Message::ConsoleMessage => {
				log!("Open Console");
				true
			}
		}
	}
}

#[function_component(Topbar)]
pub fn topbar() -> Html {
	html! {<div class={classes!("topbar")}>
		<ToggleDarkButton/>
	</div>}
}

#[function_component(ToggleDarkButton)]
pub fn toggle_dark_button() -> Html {
	let root = web_sys::window()
		.unwrap()
		.document()
		.unwrap()
		.query_selector(":root")
		.unwrap()
		.unwrap();

	let dark = use_state(|| root.class_name().ne("dark"));

	if *dark.clone() {
		let onclick = {
			let dark = dark.clone();
			Callback::from(move |_: MouseEvent| {
				root.set_class_name("dark");
				dark.set(false)
			})
		};

		html! { <div class={classes!("toggle-btn")} >
			<ElevatedButton icon={"dark_mode"} label={"dark"} {onclick} />
		</div> }
	} else {
		let onclick = {
			let dark = dark.clone();
			Callback::from(move |_: MouseEvent| {
				root.set_class_name("light");
				dark.set(true)
			})
		};
		html! {<div class={classes!("toggle-btn")} >
			<ElevatedButton icon={"light_mode"} label={"light"}  {onclick} />
		</div>}
	}
}
