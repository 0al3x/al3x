use m3rs_yew::{
	typescale::{TypescaleModifier, TypescaleRoles},
	typography::Typography,
};
use yew::prelude::*;

#[function_component(Git2P)]
pub fn git2p() -> Html {
	html! { <div class={classes!("todo")}>
		<div>
			<div><Typography
				text={"git2p project"}
				role={TypescaleRoles::Display}
				modifier={TypescaleModifier::Large}
			/></div>
			<div><Typography
				text={"an API set of modular primitives for building git-based infrastructures"}
				role={TypescaleRoles::Headline}
				modifier={TypescaleModifier::Large}
			/></div>
			<div class={classes!("")} ><Typography
				text={"coming soon"}
				role={TypescaleRoles::Body}
				modifier={TypescaleModifier::Large}
			/></div>
		</div>
	</div> }
}
