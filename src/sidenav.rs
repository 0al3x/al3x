use crate::icon_button::{IconButton, IconKind};
use crate::switch::*;
use yew::prelude::*;
use yew_router::prelude::*;

pub struct Sidenav;

impl Component for Sidenav {
	type Message = ();
	type Properties = ();

	fn create(_: &Context<Self>) -> Self {
		Self {}
	}

	fn view(&self, _: &Context<Self>) -> Html {
		let git = {
			let link = html! { <a>
				<i class="fab fa-git-alt icon"></i>
			</a>};

			html! { <div class={classes!("icon-container")}> <IconButton icon={IconKind::Link(link)} /> </div> }
		};

		html! { <div id={"drawer-sidenav-container"} >
		<nav>
			<ul>
				<li>
					<Link<Routes> to={Routes::Home}>
						<IconButton icon={ IconKind::Font(  AttrValue::from("chair")  ) } ></IconButton>
					</Link<Routes>>
				</li>
				<li>
					<Link<Routes> to={Routes::Blog}>
						<IconButton icon={ IconKind::Font(AttrValue::from("article")) } ></IconButton>
					</Link<Routes>>
				</li>

				<li>
					<Link<Routes> to={Routes::Briefcase}>
						<IconButton icon={ IconKind::Font(AttrValue::from("work")) } ></IconButton>
					</Link<Routes>>
				</li>


				<li class={"icon-bottom"} >
					<Link<Routes> to={Routes::Git2P}>
						{git}
					</Link<Routes>>
				</li>
			</ul>
		</nav>
		</div> }
	}
}
